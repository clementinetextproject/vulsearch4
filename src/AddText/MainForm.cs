using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Xml;

namespace AddText
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnNext;
		private System.Windows.Forms.Button btnCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.RichTextBox rtb;

		private ArrayList exts = new ArrayList();
		private System.Windows.Forms.Label lblProg;
		private string path="";

		public MainForm(string p)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			path=p;
			System.Drawing.Font norm=new Font("Tahoma",8);
			System.Drawing.Font bold=new Font("Tahoma",9,FontStyle.Bold);

			DirectoryInfo di=new DirectoryInfo(Application.StartupPath);
			foreach (FileInfo fi in di.GetFiles("*.xml"))
			{
				string ext=fi.Name;
				if (ext.IndexOf(".")!=-1)
					ext=ext.Substring(0,ext.LastIndexOf("."));
				exts.Add(ext);
				XmlTextReader xr=new XmlTextReader(fi.FullName);
				xr.Read();
				xr.ReadStartElement("Bible");
				string name=xr.ReadElementString("Name");
				bool isLat=bool.Parse(xr.ReadElementString("IsLatin"));
				string desc=xr.ReadElementString("Description");
				xr.Close();

				if (rtb.Text.Length>0) rtb.SelectedText="\n\n";
				rtb.SelectionFont=bold;
				rtb.SelectedText=name + " (." + ext+")\n";
				rtb.SelectionFont=norm;
				rtb.SelectedText=desc;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MainForm));
			this.label1 = new System.Windows.Forms.Label();
			this.rtb = new System.Windows.Forms.RichTextBox();
			this.btnNext = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lblProg = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(328, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "This program will add the following Bible(s) to VulSearch:";
			// 
			// rtb
			// 
			this.rtb.Location = new System.Drawing.Point(16, 32);
			this.rtb.Name = "rtb";
			this.rtb.ReadOnly = true;
			this.rtb.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
			this.rtb.Size = new System.Drawing.Size(368, 120);
			this.rtb.TabIndex = 1;
			this.rtb.Text = "";
			// 
			// btnNext
			// 
			this.btnNext.Location = new System.Drawing.Point(312, 160);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new System.Drawing.Size(64, 24);
			this.btnNext.TabIndex = 2;
			this.btnNext.Text = "Next >>";
			this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(248, 160);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(56, 24);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// lblProg
			// 
			this.lblProg.Location = new System.Drawing.Point(24, 168);
			this.lblProg.Name = "lblProg";
			this.lblProg.Size = new System.Drawing.Size(216, 16);
			this.lblProg.TabIndex = 4;
			// 
			// MainForm
			// 
			this.AcceptButton = this.btnNext;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(394, 200);
			this.Controls.Add(this.lblProg);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnNext);
			this.Controls.Add(this.rtb);
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.Text = "Install new Bible - VulSearch";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			try {
				Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(
					"Software\\VulSearch4",true); 

				if (key != null) {
					string p=(string) key.GetValue("Path");
					if(p.Length>0) {
						MainForm m= new MainForm(p);
						m.ShowDialog();
						return;
					}
				}
			}
			catch (Exception e) {
			}
			MessageBox.Show("You must install VulSearch 4 and run it at least once before you can add a Bible. You can download VulSearch 4 freely from http://vulsearch.sf.net");
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Hide();
		}

		private void btnNext_Click(object sender, System.EventArgs e)
		{
			if (btnNext.Text=="Finish")
				this.Hide();
			else
			{
				foreach (object o in exts)
				{
					string ext=(string) o;
					string p=Application.StartupPath;
					if (File.Exists(path + "\\Text\\" + ext + ".xml"))
					{
						DialogResult r=MessageBox.Show("A Bible with extension "
							+ ext + " is already installed. Do you want to over-write it?","VulSearch 4",MessageBoxButtons.YesNo);
						if (r==DialogResult.No) continue;
					}
					fileCopy(p+ "\\" + ext +".xml",path+"\\Text\\" + ext + ".xml");
					fileCopy(p+"\\" + ext +".idx",path+"\\Swish\\" + ext + ".idx");
					fileCopy(p+"\\" + ext +".idx.prop",path+"\\Swish\\" + ext + ".idx.prop");
					DirectoryInfo di=new DirectoryInfo(p);
					foreach (FileInfo fi in di.GetFiles("*." + ext))
					{
						fileCopy(fi.FullName,path+"\\Text\\" + fi.Name);
					}
				}
				lblProg.Visible=false;
				label1.Enabled=false;
				rtb.Enabled=false;
				btnCancel.Enabled=false;
				btnNext.Text="Finish";
				btnNext.Focus();
			}
		}

		private void fileCopy(string source, string dest)
		{
			lblProg.Text=dest;
			Application.DoEvents();
			try
			{
				if (File.Exists(source))
				{
					File.Copy(source,dest,true);
				}
			}
			catch (Exception e)
			{
				MessageBox.Show("Unable to copy the file " + source +
					": the following error occurred\n" + e.Message,"VulSearch 4");
			}
		}

	}
}
